package dadp02;
import java.io.*;
import java.util.*;

public class EntradaSortida{
  public static void main(String args[]) throws IOException {
    int j;
    int enter;
    int nomFicher;
    byte[] buffer= new byte[80];
    String filename, filename2;
    float f1= (float) 3.1416;
    float f2= 0;
    String fIntern2 = "";
    try {
      // E/S amb InputStream i OutputStream
      System.out.println("Teclege una cadena");
      j= System.in.read(buffer);
      System.out.print("La cadena: ");
      System.out.write(buffer,0,j);
      // Conversio de la cadena de bytes a cadena de caracters (2 bytes)
      String tira= new String(buffer,0,j);
      System.out.println("Altra vegada la cadena: " + tira);
      // E/S amb BufferedReader y PrintWriter
      // Convenient amb cadenes de caracters (1 caracter = 2 bytes)
      BufferedReader stdIn=new BufferedReader
          (new InputStreamReader(System.in));
      PrintWriter stdOut= new PrintWriter(System.out);
      // E/S amb InputStream i OutputStream
/* COMPLETAR: Llegir un enter per teclat i escriure'l en pantalla */
/* 1 */
      
      System.out.println("Teclege una enter");
      enter= System.in.read(buffer);
      System.out.print("L'enter: ");
      System.out.write(buffer,0,enter);
      System.out.println();

/* 1 */
      
      
/* COMPLETAR: Llegir una cadena per al nom del titxer des del teclat
 * emmagamatzemar-la a la variable filename, i mostrar la variable per pantalla
 */
/* 2 */
      
      // E/S amb BufferedReader i PrintWriter
      // Convenient amb cadenes de caracters (1 caracter = 2 bytes)
      System.out.println("Teclege un nom per a un fitxer");
      nomFicher= System.in.read(buffer);
      filename = new String(buffer,0,nomFicher);
      System.out.print("El nom del fitxer: " + filename );
      System.out.println();

/* 2 */
      // E/S amb fitxers i floats en format numeric
/* COMPLETAR: Escriga un float al fitxer filename (en format binari)
 * Llija el float que ha escrit al fitxer filename i mostre'l per pantalla
 * AJUDA: Mire el codi de mes avall, que us paregut pero en format de text
 */
/* 3 */
      
      
      Scanner teclat = new Scanner(System.in);
      System.out.println("Introdueix un nombre en format float (Decimal separat per una coma) al fitxer: ");
      f2 = teclat.nextFloat();
      System.out.println("El valor que ha introduit es: ");
      System.out.printf("%.3f", f2);
      System.out.println();
      
      int intBits = Float.floatToIntBits(f2);
      String binary = Integer.toBinaryString(intBits);
      
      System.out.println("El valor que ha introduit abans, es equivalent a " + binary + " en format binari!");
      teclat.close();
      
      System.out.println("Fitxer: " + filename);
      PrintWriter fout= new PrintWriter(new FileOutputStream(filename));
      /*BufferedReader fin= new BufferedReader
          (new InputStreamReader(new FileInputStream(filename)));
      fout.println(new Float(f1).toString()); fout.flush();*/
      
      BufferedReader fin= new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
      fout.println(binary); fout.flush();
      
      fIntern2 = fin.readLine();
      
      System.out.println("El float está llegit i convertit a binari: " + fIntern2 + " desde el fitxer " + filename);
      
      
/* 3 */
      // E/S amb fitxers i floats en format de text
      filename2=filename;
      System.out.println("Fitxer: "+filename2 + ".txt");
      //fout= new DataOutputStream(new FileOutputStream(filename2));
      //fin= new DataInputStream(new FileInputStream(filename2));
      //fout.writeBytes(new Float(f1).toString()+"\n");
      //f2= Float.valueOf(fin.readLine()).floatValue();
      PrintWriter fout2= new PrintWriter(new FileOutputStream(filename2));
      BufferedReader fin2= new BufferedReader
          (new InputStreamReader(new FileInputStream(filename2)));
      fout2.println(new Float(f1).toString()); fout2.flush();
      //System.out.println(fin2.readLine());
      f2= Float.valueOf(fin2.readLine()).floatValue();
      System.out.println("Escrit i llegit el float: " +f2+
          " del fitxer: " +filename2 + ".txt");
      fout.close();
      fin.close();
      fout2.close();
      fin2.close();
    } catch (IOException e) {
      System.out.println("Error en E/S");
      System.exit(1);
    } 
  }
}
