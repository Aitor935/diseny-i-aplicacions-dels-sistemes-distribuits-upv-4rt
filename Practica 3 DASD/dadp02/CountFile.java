package dadp02;
import java.io.*;

public class CountFile {
	
  public static void main (String[] args) throws java.io.IOException, java.io.FileNotFoundException {
    
	  int count= 0;
	  InputStream is;
	  String filename;
	  
	  if (args.length >= 1) {
		  /* COMPLETAR: is= Cree una instància de FileInputStream
		   * per a llegir del fitxer passat com argument a args[0]
		   */
		  /* 1 */
		  
		  filename = args[0];
		  
		  is = new FileInputStream(filename);
		  
		  /* 1 */
		  
	  } else {
		  is= System.in;
		  filename= "Entrada";
	  }

	  /* COMPLETAR: Utilitze amb while (...) count++;
	   * un mètode de FileInputSream per a llegir un caracter
	   */
	  /* 2 */
	  
	  while(is.available() != 0){
		  
		  int llegir = is.read();
		  System.out.println(llegir);
		  count++;
		  
	  }

	  /* 2 */
    
	  is.close();
	  System.out.println(filename + " tiene " + count + " caracteres.");
  }
}
