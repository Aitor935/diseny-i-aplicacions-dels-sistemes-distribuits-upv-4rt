package prova;

import java.io.*;
import java.net.*;
import java.util.*;

public class Echo {
	
  private static EchoObject ss;
  
  public static void main(String[] args) {
	  /* COMPLETAR: Cree una instància de ss */
	  /* 1 */
	  	ss = new EchoObject();
	  /* 1 */
	  	BufferedReader stdIn= new BufferedReader(new InputStreamReader(System.in));
	  	PrintWriter stdOut= new PrintWriter(System.out);
	  	String input,output;
	  	try {
	  		stdOut.print("> "); 
	  		stdOut.flush();
	  		
	  /* COMPLETAR: En bucle infinit, llegir del teclat, invocar ss.echo i imprimir */
	  /* 2 */
	  	
	  		Scanner teclat = new Scanner(System.in);	
	  		String entrada = null;
	  		
	  		while(true){
	  			
	  			entrada = teclat.nextLine();
	  			System.out.println(ss.echo(entrada)); 		
	  		}
	  
	  /* 2 */
	  	}catch(IOException e){}
  }
}
