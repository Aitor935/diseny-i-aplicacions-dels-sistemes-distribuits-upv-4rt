#!/bin/bash
#Localitzar el classpath
if [ $# -lt 2 ]
then
	echo "Falta algún paràmetre. Exemple d'execució correcta: ./scriptClient.sh localhost 45 "
	exit	
fi
a=`pwd`

#Crear el arxiu de politiques automàticament al llençar el script del Client
rm client.policy 2> /dev/null
echo "grant codeBase \"file://$a\" {" > client.policy
echo "permission java.security.AllPermission;" >> client.policy
echo "};" >> client.policy

java -cp . -Djava.rmi.server.codebase=file://$a -Djava.security.policy=client.policy client.ComputePi $1 $2
