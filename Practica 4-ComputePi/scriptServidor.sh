#!/bin/bash
#Localitzar el classpath

if [ $# -lt 1 ]
then
	echo "Falta algún paràmetre. Exemple d'execució : ./scriptServidor.sh localhost"
	exit	
fi

a=`pwd`

#Crear el arxiu de politiques automàticament al llençar el script del Servidor
rm server.policy 2> /dev/null
echo "grant codeBase \"file://$a\" {" > server.policy
echo "permission java.security.AllPermission;" >> server.policy
echo "};" >> server.policy

echo "Servidor obert en $1"
java -cp . -Djava.rmi.server.codebase=file://$a -Djava.rmi.server.hostname=$1 -Djava.security.policy=server.policy engine.ComputeEngine
