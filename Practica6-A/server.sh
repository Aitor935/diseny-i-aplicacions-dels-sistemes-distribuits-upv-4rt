#!/bin/bash
# -*- ENCODING: UTF-8 -*-

#Si no hay argumentos se ponen los valores por defecto
#Al pasar parametros, el primero es el host utilizado y el segundo corresponde a la carpeta donde se han colocado los archivos de politicas de seguridad
if [ $# -lt 2 ]
then
  host="localhost"
  dir="rmi/"
else
  host=$1
  dir=$2
fi

#Controlar el separador de directorio en el parámetro para utilizar en la ruta
if [ `echo $dir | grep -c "/" ` -lt 1 ]
then
  dir+="/"
fi

#Nombre de los archivos de políticas de seguridad
serverPolicyName=$dir
serverPolicyName+="server.policy"

java -cp ./ -Djava.rmi.server.codebase=file:server/ -Djava.rmi.server.hostname=$host -Djava.security.policy=$serverPolicyName server/EchoObjectRMI &

