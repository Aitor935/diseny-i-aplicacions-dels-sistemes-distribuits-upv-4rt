package prova;

import java.net.*;
import java.io.*;
import java.util.Timer;
import java.util.TimerTask;

import prova.EchoObjectStubT.TimeOut.TimeoutTask;



public class EchoObjectStubT implements EchoInt {
	
  private Socket echoSocket= null;
  private PrintWriter os= null;
  private BufferedReader is= null;
  private String host= "localhost";
  private int port= 7;
  private String output= "Error";
  private boolean connectat= false;
  
  private TimeoutTask tt = null;
  private boolean timeOutActivat = false;
  
  TimeOut timeOut = null;
  
  public void setHostAndPort(String host, int port) {
    this.host= host;
    this.port= port;
    timeOut = new TimeOut(10,this); 
  }
  
  public String echo(String input) throws java.rmi.RemoteException {
	  
    connecta();
    if (echoSocket != null && os != null && is != null) {
      try {
        os.println(input);
        os.flush();
        output= is.readLine();
      } catch (IOException e) {
        System.err.println("Excepció llegint/escrivint socket");
      }
    }
    ProgramarDesconexio();
    return output;
  }
  
  private synchronized void connecta() throws java.rmi.RemoteException {
    try {
    	
      if (!connectat) {
    	
    	  	echoSocket = new Socket(host,port);
    	  	is = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
    	  	os = new PrintWriter(echoSocket.getOutputStream(), true);
 
    	  	connectat= true;
    	  	System.out.println("Connectat al server");
    	  	
    	  	/* COMPLETAR: Afegisca el codi que considere necessari per completar el if */
    	  	/* 1 */
    	  	this.ProgramarDesconexio();
    	  	/* 1 */
      }
    } catch (Exception e) {
      throw new java.rmi.RemoteException();
    }
  }
  
  private synchronized void desconnecta(){ 
    try {
    
    	/* COMPLETAR: Tanque les connexions necessàries */
    	/* 2 */
    		is.close();
    		os.close();
    		echoSocket.close();
    	/* 2 */
    		connectat= false;
    		System.out.println("Desconnectat del server");
    } catch (IOException e) {
      System.err.println("Error tancant socket");
    }
  }
  
  
  private synchronized void ProgramarDesconexio(){
	  
	  timeOut.start();
  }
  
  
  public class TimeOut{
	  
	  Timer timer;
	  EchoObjectStubT stub;
	  int numSegons;
	  
	  public TimeOut(int numSegons, EchoObjectStubT stub){
		  
		  this.numSegons = numSegons;
		  this.stub = stub;
	  }
	  
	  public void start(){
		  
		  if(timeOutActivat){
			  
			  this.cancel();
		  }
		  
		  timeOutActivat = true;
		  
		  tt= new TimeoutTask();
		  timer.schedule(tt, numSegons*1000);
	  }
	  
	  public void cancel(){
		 
		  timer.cancel();
	  }
	  
	  class TimeoutTask extends TimerTask{
		  
		  public void run(){
			  
			  timeOutActivat = true;
			  desconnecta();
		  }
	  }	  
   }
}