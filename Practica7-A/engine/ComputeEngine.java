package engine;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import compute.Compute;
import compute.Task;

public class ComputeEngine implements Compute {
	
	Task cache = null;

    public ComputeEngine() {
        super();
    }

    public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            String name = "Compute";
            Compute engine = new ComputeEngine();
            Compute stub =
                (Compute) UnicastRemoteObject.exportObject(engine, 0);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);
            System.out.println("ComputeEngine bound");
        } catch (Exception e) {
            System.err.println("ComputeEngine exception:");
            e.printStackTrace();
        }
    }

	@Override
	public void loadTask(Task t) throws RemoteException {
		System.out.println("Algú ha carregat una tesk");
		cache = t;
		
	}

	@Override
	public Object executeTask(Object arg) throws RemoteException {
		System.out.println("Començant a executar la TASK");
		Object resultat = null;
		
		if( cache == null){
			resultat = "Error : Task no carregada";
			return resultat;
		}
		
		resultat = cache.execute(arg.toString());
		System.out.println("Task Executada , resposta enviada\n-------");
		
		return resultat;		
	}
}
