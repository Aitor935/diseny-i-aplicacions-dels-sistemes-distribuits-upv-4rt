package client;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import compute.Compute;

import java.awt.Graphics;
import java.applet.Applet;
public class CliApplet extends Applet {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	Object resultat = null;
	  String urlRemot= "rmi://localhost/"; 

  public void init() {
    try {
        String name = "Compute";
        Compute comp = (Compute)Naming.lookup(urlRemot+name);

       
        //Crear la task en local
        EchoTask task = new EchoTask();
        
        //Enviar la task al servidor
        comp.loadTask(task);
        
        //Executar la task, que el servidor la tindrà guardada.
        resultat = comp.executeTask("TirantDesdeAPPLET");
            
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
  public void paint(Graphics g) {
    g.drawString(resultat.toString(),25,50);
  }
}
