package client;

import compute.Task;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EchoTask implements Task<Object>, Serializable {

	private static final long serialVersionUID = 227L;

	public EchoTask() {
	}
	
	public Object execute(Object x) {
		String s = x.toString();
		String missatge;
		// Traure propietats del sistema.
		//I passar-li el  toUpperCase la String enviada.
		
		String a = System.getProperty("os.name").toString();
		String b = System.getProperty("os.version").toString();
		String c = System.getProperty("os.arch").toString();
		
		   DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		   //get current date time with Date()
		   Date date = new Date();
		   String d = dateFormat.format(date).toString();
		   
		missatge = a + "\n" + b + "\n" + c + "\n" + d + "\n------\nAdicionalment:\n"
		+s+"--toUpperCase-->"
		+s.toUpperCase();
		return missatge;
	}




}
