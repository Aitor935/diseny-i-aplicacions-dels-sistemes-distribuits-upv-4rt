package client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.math.BigDecimal;
import compute.Compute;

public class ComputePi {
    public static void main(String args[]) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            String name = "Compute";
            Registry registry = LocateRegistry.getRegistry(args[0]);
            Compute comp = (Compute) registry.lookup(name);
           
            //Crear la task en local
            EchoTask task = new EchoTask();
            
            //Enviar la task al servidor
            comp.loadTask(task);
            
            //Executar la task, que el servidor la tindrà guardada.
            Object resultat = comp.executeTask(args[1]);
            
            
            System.out.println("Resposta del Servidor:\n"+ resultat.toString());

            
        } catch (Exception e) {
            System.err.println("ComputePi exception:");
            e.printStackTrace();
        }
    }    
}
