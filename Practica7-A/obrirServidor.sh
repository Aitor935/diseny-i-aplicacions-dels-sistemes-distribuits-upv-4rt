#!/bin/bash

if [ $# -lt 1 ]
then
	echo "Falten paràmetres. Exemple : ./obrirServidor.sh localhost"
	exit	
fi

#Tancar rmiregistry's, si tenim algun
killall rmiregistry 2> /dev/null
rmiregistry &


#Localitzar el classpath (directori on tenim tot, que es el direc. actual)
a=`pwd`
rm server.policy 2> /dev/null
echo "grant codeBase \"file://$a\" {" > server.policy
echo "permission java.security.AllPermission;" >> server.policy
echo "};" >> server.policy

echo "Servidor obert en $1"
java -cp . -Djava.rmi.server.codebase=file://$a -Djava.rmi.server.hostname=$1 -Djava.security.policy=server.policy engine.ComputeEngine
killall rmiregistry

