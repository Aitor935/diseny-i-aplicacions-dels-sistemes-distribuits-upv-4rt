package engine;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import compute.Compute;
import compute.Task;

public class ComputeEngine implements Compute {
  private Task<Object> task;

  public ComputeEngine() {
    super();
  }

  @Override
  public void loadTask(Task t) throws RemoteException {
    // Carregar una nova task al ComputeEngine. No executar-la
    this.task = t;
  }

  // Executar una tasca prèviament carregada amb loadTask
  // admetent com arguments els donats en arg i tornant el resultat de Task
  // com resultat de executeTask
  @Override
  public Object executeTask(Object arg) throws RemoteException {
    return task.execute(arg);
  }

  public static void main(String[] args) {
    if (System.getSecurityManager() == null) {
      System.setSecurityManager(new SecurityManager());
    }

    try {
      String name = "Compute";
      Compute engine = new ComputeEngine();
      Compute stub = (Compute) UnicastRemoteObject.exportObject(engine, 0);
      Registry registry = LocateRegistry.getRegistry();
      registry.rebind(name, stub);
      System.out.println("ComputeEngine bound");
    }
    catch (RemoteException e) {
      System.err.println("ComputeEngine exception:");
      e.printStackTrace();
    }
    catch (Exception e) {
      System.err.println("Other exception:");
      e.printStackTrace();
    }
  }
}
