package engine;

import java.io.Serializable;
import java.net.*;
import java.io.*;
import java.text.*;
import java.util.*;
import rmi.EchoInt;
import compute.Task;
import java.math.BigDecimal;

public class EchoObject implements Task<String>, EchoInt, Serializable {
  String myURL="localhost";
  public EchoObject(){
    try {
      myURL=InetAddress.getLocalHost().getHostName();
    }
    catch (UnknownHostException e) {
      myURL="localhost";
    }
  }

  public String echo(String input) throws java.rmi.RemoteException {
    Date h = new Date();
    String fecha = DateFormat.getTimeInstance(3,Locale.FRANCE).format(h);
    String ret = myURL + ":" + fecha + "> " +  input;
    try {
      Thread.sleep(3000);  ret = ret + " (retrasada 3 segundos)";
    }
    catch (InterruptedException e) {}
    return ret;
  }

  public String execute(String input) {
    try {
      if(Integer.parseInt(input) < 0) {
        // Si el número es menor a 0 se establecen 4 digitos decimales por defecto
        input = ""+4;
      }
    }
    catch(NumberFormatException e) {
      // Si el argumento no es numérico se establecen 4 dígitos decimales por defecto
      input = ""+4;
    }

    String ret="Resultado: ";
    try {
      Thread.sleep(3000);

      Pi pi = new Pi(Integer.parseInt(input));
      BigDecimal num = pi.execute();
      ret += num.toString();
    }
    catch (InterruptedException e) { }
    return ret;
  }
}
