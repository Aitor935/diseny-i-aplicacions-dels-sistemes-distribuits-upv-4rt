package client;

import java.io.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import compute.Compute;
import engine.EchoObject;

public class ComputePi {
  public static void main(String[] args) {
    if (args.length < 1) {
      System.out.println("Us: ComputePi host");
      System.exit(1);
    }

    if (System.getSecurityManager() == null) {
      System.setSecurityManager(new SecurityManager());
    }

    BufferedReader stdIn= new BufferedReader(new InputStreamReader(System.in));
    PrintWriter stdOut= new PrintWriter(System.out);
    String input,output;

    try {
      String name = "Compute";
      Registry registry = LocateRegistry.getRegistry(args[0]);
      Compute comp = (Compute) registry.lookup(name);

      EchoObject task = new EchoObject();
      comp.loadTask(task);

      stdOut.print("> ");
      stdOut.flush();
      while ((input= stdIn.readLine()) != null) {
        output = (String)comp.executeTask(input);
        stdOut.println(output);
        stdOut.print("> "); stdOut.flush();
      }
    }
    catch (Exception e) {
      System.err.println("ComputePi exception:");
      e.printStackTrace();
    }
  }
}
