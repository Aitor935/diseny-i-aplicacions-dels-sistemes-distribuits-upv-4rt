package prova;

import java.io.*;
import java.net.*;

public class Echo2 {
	
  private static EchoObjectStub ss;
  
  public static void main(String[] args) {
    
	  if (args.length < 2) {
		  
		  System.out.println("Us: Echo2 host port");
		  System.exit(1);
	  }

	  /* COMPLETAR: Cree una instància de ss i invoque ss.setHostAndPort 
              de manera que faça ús del host i port passats com arguments (args[0] i args[1]) */
	  /* 1 */
	  	ss = new EchoObjectStub();
	  	ss.setHostAndPort(args[0].toString(), Integer.parseInt(args[1]));
	  /* 1 */
	  	BufferedReader stdIn= new BufferedReader(new InputStreamReader(System.in));
	  	PrintWriter stdOut= new PrintWriter(System.out);
	  	String input,output;
	  
	  try {
		  
		  stdOut.print(" > "); stdOut.flush();
		  
		  while ((input= stdIn.readLine()) != null) {
			  output= ss.echo(input);
			  stdOut.println(output);
			  stdOut.print("> "); stdOut.flush();
		  }
		  
    } catch (IOException e) {}
  }
}
